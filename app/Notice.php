<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notice extends Model
{

	//fillable fields for new notice

	protected $fillable = [
		'provider_id',
		'infringing_title',
		'infringing_link',
		'original_link',
		'original_description',
		'template',
		'content_removed',
	];

	//open a new notice
    public static function open(array $attributes)
    {
    	return new static($attributes);
    }


    //set the email template for the notice

    public function useTemplate($template)
    {
    	$this->template = $template;

    	return $this;
    }
}
