<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Provider;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Guard;
use App\Notice;

class NoticesController extends Controller
{

	//Create a new notices controller instance

	public function __construct()
	{
		$this->middleware('auth');
	}

	//Show all notices

    public function index()
    {
    	return 'all notices';
    }


    //Show a page to create a new notice

    public function create()
    {

    	//get list of providers
        $providers = Provider::lists('name', 'id');
    	// load a view to create a new notice

    	return view('notices.create', compact('providers'));

    }

    public function confirm(Requests\PrepareNoticeRequest $request, Guard $auth)
    {

        $template = $this->compileDmcaTemplate($data = $request->all(), $auth);

        session()->flash('dmca', $data);

        return view('notices.confirm', compact('template'));

    }

    public function store(Request $request)
    {
        $data = session()->get('dmca');

        Notice::open($data)
            ->useTemplate($request->input('template'))
            ->save();
    }

    public function compileDmcaTemplate($data, Guard $auth)
    {
        $data = $data + [
            'name' => $auth->user()->name,
            'email' => $auth->user()->email,
        ];

        return view()->file(app_path('Http/Templates/dmca.blade.php'), $data);
    }
}

